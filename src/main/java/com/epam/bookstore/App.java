package com.epam.bookstore;

import com.epam.bookstore.controller.BookController;

public class App {

    private static final String QUERY = "query.txt";

    private final BookController controller;

    public App() {
        this.controller = new BookController();
    }

    public static void main(String[] args) {
        App app = new App();
        System.out.println(app.controller.generateDataFiles(QUERY));
    }


}
