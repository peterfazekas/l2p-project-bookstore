package com.epam.bookstore.controller;

import com.epam.bookstore.domain.Book;
import com.epam.bookstore.domain.BookInstance;
import com.epam.bookstore.google.DataReader;
import com.epam.bookstore.google.factory.BookFactory;
import com.epam.bookstore.google.factory.BookInstanceFactory;
import com.epam.bookstore.google.parser.*;
import com.epam.bookstore.google.service.GoogleBookJsonParser;
import com.epam.bookstore.google.service.GoogleBookReader;
import com.epam.bookstore.service.*;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BookController {

    private static final String MESSAGE = "Exit with success. Output files are generated." ;

    private final DataReader reader;
    private final DataWriter logBook;
    private final DataWriter sqlBook;
    private final DataWriter logBookInstances;
    private final DataWriter sqlBookInstances;
    private final BookInstanceFactory bookInstanceFactory;

    public BookController() {
        JsonNodeValueFacade jsonNodeValueFacade =
                new JsonNodeValueFacade(new JsonNodeValueParser(), new AuthorParser(),
                        new IsbnParser(), new BookCoverUrlParser());
        reader = new DataReader(new GoogleBookReader(),
                new GoogleBookJsonParser(new JsonParser(), new BookFactory(jsonNodeValueFacade)));
        logBook = new CsvFileWriter();
        sqlBook = new SqlBookWriter();
        logBookInstances = new CsvFileWriter();
        sqlBookInstances = new SqlBookInstanceWriter();
        bookInstanceFactory = new BookInstanceFactory();
    }

    public String generateDataFiles(final String categoryName) {
        List<String> queries = readCategories(categoryName);
        List<Book> books = new ArrayList<>();
        queries.forEach(query -> books.addAll(reader.getBooks(query)));
        List<BookInstance> bookInstances = bookInstanceFactory.create(books);
        createOutputFiles(Arrays.asList(logBook, sqlBook), books);
        createOutputFiles(Arrays.asList(logBookInstances, sqlBookInstances), bookInstances);
        return MESSAGE;
    }

    private List<String> readCategories(final String fileName) {
        return new QueryReader().read(fileName);
    }

    private <T> void createOutputFiles(final List<DataWriter> dataWriter, final List<T> list) {
        dataWriter.forEach(writer -> writer.write(list));
    }


}
