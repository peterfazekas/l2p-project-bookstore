package com.epam.bookstore.domain;

public class Book {

    private static final String SEPARATOR = ";";
    private static final Integer MAXIMUM_STOCK = 5;

    private final long id;
    private final String title;
    private final String summary;
    private final String author;
    private final String year;
    private final String isbn;
    private final String bookUrl;
    private final String coverUrl;
    private final Integer maximumStock;

    public Book(final Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.summary = builder.summary;
        this.author = builder.author;
        this.year = builder.year;
        this.isbn = builder.isbn;
        this.bookUrl = builder.bookUrl;
        this.coverUrl = builder.coverUrl;
        this.maximumStock = (builder.maximumStock == null) ? MAXIMUM_STOCK : builder.maximumStock;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public String getAuthor() {
        return author;
    }

    public String getYear() {
        return year;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getBookUrl() {
        return bookUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public Integer getMaximumStock() {
        return maximumStock;
    }

    @Override
    public String toString() {
        return id + SEPARATOR + isbn + SEPARATOR + author + SEPARATOR + title + SEPARATOR + summary + SEPARATOR +
                year + SEPARATOR + bookUrl + SEPARATOR + coverUrl + SEPARATOR + maximumStock + SEPARATOR + maximumStock;
    }

    public static final class Builder {
        private long id;
        private String title;
        private String summary;
        private String author;
        private String year;
        private String isbn;
        private String bookUrl;
        private String coverUrl;
        private Integer maximumStock;

        public Builder withId(long id) {
            this.id = id;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withSummary(String summary) {
            this.summary = summary;
            return this;
        }

        public Builder withAuthor(String author) {
            this.author = author;
            return this;
        }

        public Builder withYear(String year) {
            this.year = year;
            return this;
        }

        public Builder withIsbn(String isbn) {
            this.isbn = isbn;
            return this;
        }

        public Builder withBookUrl(String bookUrl) {
            this.bookUrl = bookUrl;
            return this;
        }

        public Builder withCoverUrl(String coverUrl) {
            this.coverUrl = coverUrl;
            return this;
        }

        public Builder withMaximumStock(Integer maximumStock) {
            this.maximumStock = maximumStock;
            return this;
        }

        public Book build() {
            return new Book(this);
        }
    }
}
