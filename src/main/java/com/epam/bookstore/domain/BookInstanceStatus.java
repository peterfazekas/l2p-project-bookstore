package com.epam.bookstore.domain;

public enum BookInstanceStatus {
    AVAILABLE, LOCKED, RENTED
}
