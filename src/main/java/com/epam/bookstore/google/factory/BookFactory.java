package com.epam.bookstore.google.factory;

import com.epam.bookstore.domain.Book;
import com.epam.bookstore.google.parser.JsonNodeValueFacade;
import com.epam.bookstore.util.StringUtils;
import com.google.gson.JsonObject;

public class BookFactory {

    private final JsonNodeValueFacade jsonNodeValue;
    private static long id;

    public BookFactory(JsonNodeValueFacade jsonNodeValue) {
        this.jsonNodeValue = jsonNodeValue;
    }

    public Book create(final JsonObject volumeInfo) {
        return new Book.Builder()
                .withId(++id)
                .withTitle(jsonNodeValue.getTitle(volumeInfo))
                .withSummary(jsonNodeValue.getSummary(volumeInfo))
                .withAuthor(jsonNodeValue.getAuthor(volumeInfo))
                .withYear(jsonNodeValue.getYear(volumeInfo))
                .withIsbn(jsonNodeValue.getIsbn(volumeInfo))
                .withBookUrl(jsonNodeValue.getBookInfoUrl(volumeInfo))
                .withCoverUrl(jsonNodeValue.getBookCoverUrl(volumeInfo))
                .build();
    }

    public boolean hasIsbnValue(final JsonObject isbn) {
        return StringUtils.hasValue(jsonNodeValue.getIsbn(isbn));
    }


}
