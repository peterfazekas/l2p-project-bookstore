package com.epam.bookstore.google.factory;

import com.epam.bookstore.domain.Book;
import com.epam.bookstore.domain.BookInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class BookInstanceFactory {

    private long id;

    public List<BookInstance> create(final List<Book> books) {
        List<BookInstance> bookInstances = new ArrayList<>();
        books.stream()
                .map(this::createBookInstances)
                .forEach(bookInstances::addAll);
        return bookInstances;
    }

    private List<BookInstance> createBookInstances(final Book book) {
        return IntStream.range(0, book.getMaximumStock())
                .mapToObj(i -> createBookInstance(book))
                .collect(Collectors.toList());
    }

    private BookInstance createBookInstance(final Book book) {
        return new BookInstance.Builder()
                .withId(++id)
                .withBook(book)
                .build();
    }
}
