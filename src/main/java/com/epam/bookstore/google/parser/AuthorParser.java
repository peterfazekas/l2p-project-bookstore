package com.epam.bookstore.google.parser;

import com.epam.bookstore.util.StringUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.stream.IntStream;

public class AuthorParser implements JsonNodeValueParserStrategy {

    private static final String AUTHORS = "authors";
    private static final String AUTHOR_SEPARATOR = ", ";

    @Override
    public String getValue(final JsonObject volumeInfo) {
        String value = StringUtils.EMPTY;
        try {
            JsonArray authors = volumeInfo.get(AUTHORS).getAsJsonArray();
            StringBuilder sb = new StringBuilder(authors.get(0).getAsString());
            if (authors.size() > 1) {
                IntStream.range(1, authors.size()).forEach( i -> {
                    sb.append(AUTHOR_SEPARATOR);
                    sb.append(authors.get(i).getAsString());
                });
            }
            value = sb.toString();
        } catch (NullPointerException npe) {
        }
        return value;
    }
}
