package com.epam.bookstore.google.parser;

import com.epam.bookstore.util.StringUtils;
import com.google.gson.JsonObject;

public class BookCoverUrlParser implements JsonNodeValueParserStrategy {

    private static final String IMAGE_LINKS = "imageLinks";
    private static final String THUMBNAIL = "thumbnail";

    @Override
    public String getValue(final JsonObject volumeInfo) {
        String nodeValue = StringUtils.EMPTY;
        try {
            JsonObject imageLinks = volumeInfo.get(IMAGE_LINKS).getAsJsonObject();
            nodeValue = imageLinks.get(THUMBNAIL).getAsString();
        } catch (NullPointerException npe) {
        }
        return nodeValue;
    }
}
