package com.epam.bookstore.google.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.function.Predicate;
import java.util.stream.IntStream;

public class IsbnParser implements JsonNodeValueParserStrategy {

    private static final String INDUSTRY_IDENTIFIERS = "industryIdentifiers";
    private static final String IDENTIFIER = "identifier";
    private static final String TYPE = "type";
    private static final String ISBN_13 = "ISBN_13";

    @Override
    public String getValue(final JsonObject volumeInfo) {
        StringBuilder sb = new StringBuilder();
        try {
            JsonArray industryIdentifiers = volumeInfo.get(INDUSTRY_IDENTIFIERS).getAsJsonArray();
            IntStream.range(0, industryIdentifiers.size())
                    .mapToObj(i -> industryIdentifiers.get(0))
                    .map(JsonElement::getAsJsonObject)
                    .filter(hasIsbn13Type())
                    .map(i -> i.get(IDENTIFIER))
                    .map(JsonElement::getAsString)
                    .limit(1)
                    .forEach(sb::append);
        } catch (NullPointerException npe) {
        }
        return sb.toString();
    }

    private Predicate<JsonObject> hasIsbn13Type() {
        return node -> ISBN_13.equals(node.get(TYPE).getAsString());
    }
}
