package com.epam.bookstore.google.parser;

import com.epam.bookstore.util.StringUtils;
import com.google.gson.JsonObject;

public class JsonNodeValueParser{

    public String getValue(final JsonObject volumeInfo, final String nodeName) {
        String nodeValue = StringUtils.EMPTY;
        try {
            nodeValue = volumeInfo.get(nodeName).getAsString();
        } catch (NullPointerException npe) {
        }
        return nodeValue;
    }
}
