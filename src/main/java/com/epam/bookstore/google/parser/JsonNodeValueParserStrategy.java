package com.epam.bookstore.google.parser;

import com.google.gson.JsonObject;

public interface JsonNodeValueParserStrategy {

    String getValue(JsonObject volumeInfo);

}
