package com.epam.bookstore.service;

import java.util.List;

public abstract class DataWriter {

    public abstract <T> void write(final List<T> list);

    <T> String getFileName(final List<T> list) {
        String fileName = null;
        if (list.size() > 0) {
            String packageName = list.get(0).getClass().getCanonicalName();
            int start = packageName.lastIndexOf('.');
            fileName = packageName.substring(start + 1).toLowerCase();
        }
        return fileName;
    }

}
