package com.epam.bookstore.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class QueryReader {

    private static final List<String> QUERIES = Arrays.asList("java", "java8", "java9", "javascript", "spring_boot", "clean_code", "design_pattern");

    public List<String> read(final String fileName) {
        List<String> queries;
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            queries = br.lines().collect(Collectors.toList());
        } catch (IOException e) {
            queries = QUERIES;
        }

        return queries;
    }
}
