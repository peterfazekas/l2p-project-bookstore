package com.epam.bookstore.service;

import com.epam.bookstore.domain.Book;
import com.epam.bookstore.domain.BookInstance;

import java.util.Arrays;
import java.util.List;

public class SqlBookInstanceWriter extends SqlWriter {

    private static final List<String> BOOK_INSTANCE_TABLE = Arrays.asList(
            "create table bookinstance (",
            "     id integer,",
            "     book_id integer,",
            "     status varchar(10),",
            "     primary key (id)",
            ");");

    private static final String SQL_INSERT_PREFIX = "insert into bookinstance (id, book_id, status) values " ;

    @Override
    List<String> createSqlHeader() {
        return BOOK_INSTANCE_TABLE;
    }

    @Override
    <T> String createSqlInsertBookValue(final T entity) {
        BookInstance bookInstance = (BookInstance) entity;
        StringBuilder sb = new StringBuilder(SQL_INSERT_PREFIX);
        sb.append(OPENING_PARENTHESIS)
            .append(bookInstance.getId())
            .append(COMMA)
            .append(bookInstance.getBook().getId())
            .append(COMMA)
            .append(wrap(bookInstance.getStatus().name()))
            .append(CLOSING_PARENTHESIS);
        return sb.toString();
    }
}
