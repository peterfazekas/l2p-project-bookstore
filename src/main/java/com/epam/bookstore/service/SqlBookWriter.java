package com.epam.bookstore.service;

import com.epam.bookstore.domain.Book;

import java.util.Arrays;
import java.util.List;

public class SqlBookWriter extends SqlWriter {

    private static final List<String> BOOK_TABLE = Arrays.asList(
            "create table book (",
            "     id integer,",
            "     isbn varchar(13),",
            "     title varchar(255),",
            "     author varchar(255),",
            "     summary varchar(500),",
            "     year integer,",
            "     cover varchar(255),",
            "     url varchar(255),",
            "     currentStock integer,",
            "     maximumStock integer,",
            "     primary key (id)",
            ");");

    private static final String SQL_INSERT_PREFIX =
            "insert into book (isbn, title, author, summary, year, cover, url, currentStock, maximumStock) values " ;

    @Override
    List<String> createSqlHeader() {
        return BOOK_TABLE;
    }

    @Override
    <T> String createSqlInsertBookValue(final T entity) {
        Book book = (Book) entity;
        StringBuilder sb = new StringBuilder(SQL_INSERT_PREFIX);
        sb.append(OPENING_PARENTHESIS)
            .append(book.getId())
            .append(COMMA)
            .append(wrap(book.getIsbn()))
            .append(COMMA)
            .append(wrap(book.getTitle()))
            .append(COMMA)
            .append(wrap(book.getAuthor()))
            .append(COMMA)
            .append(wrap(book.getSummary()))
            .append(COMMA)
            .append(book.getYear())
            .append(COMMA)
            .append(wrap(book.getCoverUrl()))
            .append(COMMA)
            .append(wrap(book.getBookUrl()))
            .append(COMMA)
            .append(book.getMaximumStock())
            .append(COMMA)
            .append(book.getMaximumStock())
            .append(CLOSING_PARENTHESIS);
        return sb.toString();
    }
}
