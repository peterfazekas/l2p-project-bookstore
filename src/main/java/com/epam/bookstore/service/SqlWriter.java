package com.epam.bookstore.service;

import org.apache.commons.text.StringEscapeUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public abstract class SqlWriter extends DataWriter {

    private static final String WRAPPER = "\"";
    private static final String SQL_POST_FIX = ".sql";
    protected static final String OPENING_PARENTHESIS = "(";
    protected static final String CLOSING_PARENTHESIS = ");";
    protected static final String COMMA = ", ";

    @Override
    public <T> void write(final List<T> list) {
        String fileName = getFileName(list);
        if (fileName != null) {
            try (PrintWriter pr = new PrintWriter(new FileWriter(fileName + SQL_POST_FIX))) {
                createSqlHeader().forEach(pr::println);
                list.stream().map(this::createSqlInsertBookValue).forEach(pr::println);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    abstract List<String> createSqlHeader();

    abstract <T> String createSqlInsertBookValue(final T entity);

    String wrap(final String text) {
        return WRAPPER + StringEscapeUtils.escapeJava(text) + WRAPPER;
    }

}
