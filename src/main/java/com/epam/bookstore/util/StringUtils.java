package com.epam.bookstore.util;

public class StringUtils {

    public static final String EMPTY = "";

    public static boolean hasValue(final String text) {
        return text != null && !EMPTY.equals(text);
    }

    public static String cropYear(final String year) {
        return year.length() > 4 ? year.substring(0, 4) : year;
    }

}
